package com.epam.yurchuk.task01.controller;

import com.epam.yurchuk.task01.model.StringUtils;

import java.util.Collections;

public class Controller implements ContollerImpl{
    private String string = "testing String12324214DD";
    private String word = "Word";
    StringUtils stringUtils = new StringUtils(string);

    public String concatAllParameters() {
        return stringUtils.concatAllParameters(Collections.singletonList("da"));
    }

    public String replaceVowels(String word) {
        return stringUtils.replaceVowels(word);
    }

    public String capitalized() {
        return stringUtils.capitalized();
    }

    public String splitSomeStrings() {
        return stringUtils.splitSomeStrings();
    }
}
