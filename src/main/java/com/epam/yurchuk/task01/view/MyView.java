package com.epam.yurchuk.task01.view;

import com.epam.yurchuk.task01.controller.Controller;

import java.util.*;

public class MyView {
    private Map<String, String> menu;
    private Map<String, PrintTable> methods;
    private static Scanner scanner = new Scanner(System.in);

    private Controller controller;
    private Locale locale;
    private ResourceBundle resourceBundle;

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", resourceBundle.getString("1"));
    }

    public MyView() {
        locale = new Locale("en");
        resourceBundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        methods.put("1", controller::capitalized);
    }

    private void outputMenu() {
        System.out.println("\n MENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }
    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                methods.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
