package com.epam.yurchuk.task01;

import com.epam.yurchuk.task01.view.MyView;

public class Application {
    public static void main(String[] args) {
        new MyView().show();
    }
}
