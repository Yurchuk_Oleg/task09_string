package com.epam.yurchuk.task01.model;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {
    private String string;

    public StringUtils(String string) {
        this.string = string;
    }

    public String concatAllParameters(List<? extends CharSequence> parameters) {
        StringBuilder stringBuilder = new StringBuilder();
        for (CharSequence param : parameters) {
            String string = param.toString().replaceAll("\\s+", "");
            stringBuilder.append(string);
        }
        return stringBuilder.toString();
    }

    public String replaceVowels(String word) {
        return word.replaceAll("[aeoui]", "_");
    }

    public String capitalized() {
        Pattern pattern = Pattern.compile("[^\\.]*\\.\\s*");
        Matcher matcher = pattern.matcher(string);
        String capitalized = "", match;
        while (matcher.find()) {
            match = matcher.group();
            capitalized += Character.toUpperCase(match.charAt(0)) + match.substring(1);
        }
        return capitalized;
    }

    public String splitSomeStrings() {
        String[] strings = string.split("\\bthe|you\\b");
        StringBuilder stringBuilder = new StringBuilder();
        for (String s : strings) {
            stringBuilder.append(s);
        }
        return stringBuilder.toString().trim();
    }
}
