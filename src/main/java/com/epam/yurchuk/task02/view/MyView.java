package com.epam.yurchuk.task02.view;

import com.epam.yurchuk.task02.controller.Controller;
import com.epam.yurchuk.task02.controller.ControllerImpl;

import java.io.IOException;
import java.util.*;

public class MyView {
    private Map<String, String> menu;
    private Map<String, PrintTable> methodsMenu;
    private Map<String, String> languages;
    private static Scanner scanner = new Scanner(System.in);

    private Controller controller;
    private ResourceBundle resourceBundle;
    private ResourceBundle bundleListLanguages;

    public MyView() throws IOException {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        languages = new LinkedHashMap<>();
        this.resourceBundle = ResourceBundle.getBundle("MyMenu", new Locale("en"));
        fillMenu();
        fillMethodsMenu();
        fillLanguages();
    }

    private void fillMenu() {
        menu.put("0", " 0 - Print list languages");
        menu.put("1", "  1 - Replace All");
    }

    private void fillMethodsMenu() {
        methodsMenu.put("0", this::pressButton0);
        methodsMenu.put("1", this::pressButton1);
    }

    private void fillLanguages() {
        languages.put("en", resourceBundle.getString("1"));
    }

    private void pressButton1() {
        controller.displayReplaceAll();
    }

    private void pressButton0() {
        printListLanguages();
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception ignored) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void printListLanguages() {
        languages.forEach((key, value) -> System.out.println("`" + key + "` - " + "\t" + value));
    }
}
