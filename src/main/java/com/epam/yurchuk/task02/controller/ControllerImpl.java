package com.epam.yurchuk.task02.controller;

import com.epam.yurchuk.task02.model.BussinesLogic;

import java.io.IOException;

public class ControllerImpl implements Controller {
    private BussinesLogic bussinesLogic;

    public ControllerImpl() throws IOException {
        bussinesLogic = new BussinesLogic();
    }

    public void displayReplaceAll(){
        bussinesLogic.displayReplaceAll();
    }
}
