package com.epam.yurchuk.task02.model;

import java.util.ArrayList;
import java.util.List;

public class Sentence {

    private List<Word> words = new ArrayList<>();
    private String sentence;

    public Sentence(String sentence) {
        this.sentence = sentence;
    }

    public List<Word> getWords() {
        return words;
    }

    public void setWords(List<Word> words) {
        this.words = words;
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    @Override
    public String toString() {
        return "Sentence{" +
                "sentence='" + sentence + '\'' +
                '}';
    }

    public String replaceAll() {
        sentence = sentence.replaceAll("[():;!?,=—]", "");
        return sentence;
    }

    public void WordsToList(String text) {
        for (String wordText : text.split("[\\s]+")) {
            String word = wordText.trim();
            if (word.length() > 0) {
                this.words.add(new Word(word));
            }
        }
    }

}
