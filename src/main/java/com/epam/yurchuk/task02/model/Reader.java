package com.epam.yurchuk.task02.model;

import java.io.*;

public class Reader {

    public static String ReadText(String path) throws IOException {
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(path);
        } catch (FileNotFoundException e) {
            //Logger
        }
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
        String text = bufferedReader.readLine();
        StringBuilder stringBuilder = new StringBuilder();
        while (text != null) {
            stringBuilder.append(text).append("\n");
            text = bufferedReader.readLine();
        }
        return stringBuilder.toString();
    }
}
