package com.epam.yurchuk.task02;

import com.epam.yurchuk.task02.view.MyView;

import java.io.IOException;

public class App {
    public static void main(String[] args) throws IOException {
        new MyView().show();
    }
}
